// Import "express" and any controllers using the "require" directive.
// Routes contains all endpoints for our application
// We separate the routes such that "app.js" only contains information on our server.
// We nee to use express.Router() method/function to achive this.
const express = require("express");

// The "TaskController" allows us to use the functions defined in the "TaskController.js" file.
const TaskController = require("../controllers/TaskController.js");

// Creates a Router instance that functions as a middleware and routing system.
// Allows access to HTTP method middlewares that make it easier to create routes for our application.
const router = express.Router();



// ROUTES
// The route are responsible for defining the URIs that our client accesses and the corressponding controller functions that will be used when a route is accessed.
// When a route is accessed they invoke the controller functions.



// Route to GET task - runs the getAllTasks function from the controller.
router.get("/", (req, res) => {
    TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
});


// Route for POST tasks - runs the createTask function from the controller.
router.post("/create", (req, res) => {
    TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
});



// Route for UPDATE/PUT task

router.put("/:id/update", (req, res) => {
    console.log(req.params.id)
    TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});


// Routes for DELETE task

router.delete("/:id/delete", (req, res) => {
    TaskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController))
});


// ACTIVITY CODE BELOW:

// Routes for GET specific tasks

router.get("/:id", (req, res) => {
    TaskController.getTaskById(req.params.id).then((resultFromController) => res.send(resultFromController))
});

// Routes for PUT update status complete

router.put("/complete/:id", (req, res) => {
    console.log(req.params.id)
    TaskController.completeTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});








// Use "module.exports" to export the router object to be used in the app.js
module.exports = router;

