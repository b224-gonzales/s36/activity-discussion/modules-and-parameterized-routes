// Controllers contain the functions and business logic of our Express JS app.
// Meaning all the operations it can perform will be written in this file.

const Task = require("../models/Task.js");


// Controller Functions:

// Function for getting all the tasks from our DB.
module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result;
    })
};



// Function for creating tasks
module.exports.createTask = (reqBody) => {
    let newTask = new Task({
        name: reqBody.name
    })

    return newTask.save().then((savedTask, error) => {
        if(error) {
            console.log(error)
            return false
        } else if (savedTask != null && savedTask.name == reqBody.name) {
            return "Duplicate Task Found!"
        } else {
            console.log(savedTask);
            return savedTask
        }
    })
};


// Function for updating a task
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if(error) {
            console.log(error)
            return false
        }
            result.name = newContent.name
            return result.save().then((updateTask, error) => {
                if(error) {
                    console.log(error)
                    return false
                } else {
                    return updateTask
            }
        })
    })
}



// Function for deletiting a task

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((deletedTask, error) =>{
        if(error){
            return false // "Cannot Delete Task" or "An error has occured!"
        } else {
            return  "Task deleted successfully!" //deletedTask
        }
    })
}


// ACTIVITY CODE BELOW:

// Function for retrieving specific tasks

module.exports.getTaskById = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if(error) {
            console.log(error)
            return false
        } else {
            return result
        }
    })
}

// Function for updating status of tasks

module.exports.completeTask = (taskId, newStatus) => {
    return Task.findById(taskId).then((result, error) => {
        if(error) {
            console.log(error)
            return false
        }
            result.status = newStatus.status
            return result.save().then((completeTask, error) => {
                if(error) {
                    console.log(error)
                    return false
                } else {
                    return completeTask
            }
        })
    })
}
