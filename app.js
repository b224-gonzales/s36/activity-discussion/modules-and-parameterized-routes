// Setup Imports

const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const { application } = require('express');

// This allows us to use all the routes defined in the "taskRoutes.js" file.
const taskRoutes = require("./routes/taskRoutes.js");

// Express setup
const app = express();
const port = 3001;

// Initialize dotenv

dotenv.config();

// Middlewares

app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// Mongoose

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PW}@batch224.3jxu8dh.mongodb.net/s36?retryWrites=true&w=majority`, 
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    );

let db = mongoose.connection;

db.on('error',
    (err) => {
        console.error('Connection error');
    });
db.once('open', () => console.log('Connected to MongoDB'));



// Main URI
// http://localhost:3001/tasks/<endpoint>
app.use("/tasks", taskRoutes);








app.listen(port, () => console.log(`Server running at port ${port}`));
