const mongoose = require('mongoose');


// Create the Schema and model and eport the
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'Pending'
    }
});




module.exports = mongoose.model("Task", taskSchema);